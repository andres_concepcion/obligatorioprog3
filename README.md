La ONG Adoptame! tiene como misión favorecer la adopción de animales sin hogar, y decidió hacer un sitio web en el que publicar los animales que están en refugios o en situación de abandono.

La aplicación web debe favorecer la visibilidad de los animales (búsquedas, catalogo) independientemente del refugio, usando varios criterios, por ejemplo, la zona, o las características físicas del animal (seudónimo, tamaño, peso, edad, vacunas, castración y estado sanitario).

Un usuario web se puede registrar como “padrino”. Se le pedirán los datos básicos como ser cedula, nombre, dirección, teléfono y email. Luego de registrado se le permitirá iniciar el trámite de adopción, para ello debe elegir un animal de cierto refugio, y se debe registrar la fecha de inicio del mismo.
La adopción queda en estado “iniciada”, luego que se entrega el animal, pasa a estar “en proceso” y se da por “completada” por parte de un integrante del refugio una vez que quién lo adoptó de su conformidad, para lo que tiene un plazo de un mes después de retirado. Antes de ese plazo puede retornarlo al refugio, justificando las causas de la no-adopción, y la adopción pasa a ser “rechazada”.

La ONG podrá emitir desde la aplicación ciertos reportes:
- Animales en adopciones, filtrado por distintos estados de adopción.
- Refugios, con la cantidad de adopciones “completadas” y “rechazadas”
- Padrinos con más de una adopción en dos años

Se permitirá a cualquier visitante de la aplicación reportar un animal en situación de abandono, para ello se le exige una dirección de correo y un teléfono de contacto. Se mantiene la fecha del reporte junto con los datos de contacto y una bandera para saber si fue respondido o no.

La ONG podrá gestionar los refugios (nombre, dirección y zona). Un refugio podrá ser dado de baja mientras no tenga animales en el mismo. Además puede gestionar los reportes de los visitantes y dar de baja a algún “padrino” si se lo considera necesario. Un integrante de un refugio debe gestionar las adopciones y los animales (de los cuales además se tiene una fecha de deceso para los que ya no nos acompañan). Un “padrino” registrado podrá modificar sus propios datos desde la aplicación.

Se debe contar con una aplicación de escritorio para gestionar los usuarios de la Ong y de los refugios. Se mantendrá un identificador autonumérico, cedula, nombre y fecha de ingreso. Sólo un administrador perteneciente a Adoptame! podrá realizar esta gestión.
Esta aplicación de escritorio publicará además en un web service los datos de login de los usuarios, siendo la aplicación web quién consuma este servicio para la autenticación.