﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppEscritorio
{
    public partial class GestionONG : Form
    {
        public GestionONG()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            try
            {
                int usuario = int.Parse(txtUsuarioLogin.Text);
                string pass = txtPasswordLogin.Text;
                if (AccesoControllerEmpleado.getInstancia().inicioSesion(usuario,pass)=="Admin")
                {
                    btnEmpleado.Visible = true;
                    lblUsuarioLogin.Visible = false;
                    lblPasswordLogin.Visible = false;
                    txtUsuarioLogin.Visible = false;
                    txtPasswordLogin.Visible = false;
                    btnEntrar.Visible = false;
                }
                else
                {
                    txtUsuarioLogin.Text = "";
                    txtPasswordLogin.Text = "";
                    MessageBox.Show("El usuario y/o contrasenia son incorrectos");
                }
            }
            catch
            {
                txtUsuarioLogin.Text = "";
                txtPasswordLogin.Text = "";
                MessageBox.Show("El usuario y/o contrasenia son incorrectos");
            }
        }

        private void btnEmpleado_Click(object sender, EventArgs e)
        {
            panelEmpleados.Visible = true;
        }

        private void btnBuscarEmpleado_Click(object sender, EventArgs e)
        {
            sControllerEmpleado.Empleado emp = AccesoControllerEmpleado.getInstancia().buscarEmpleadoPorId(int.Parse(txtIdEmpleado.Text));
            if (emp != null)
            {
                txtCiEmpleado.Text = emp.Ci.ToString();
                txtNombreEmpleado.Text = emp.Nombre;
               // dateFechaIngresoEmpleado.Value = emp.FechaIngreso;
                ddlEstadoEmpleado.SelectedValue = emp.Estado;
            }
            else
            {
                txtNombreEmpleado.Text = "El empleado no existe";
            }
        }

        private void btnAgregarEmpleado_Click(object sender, EventArgs e)
        {
            bool aux = AccesoControllerEmpleado.getInstancia().altaEmpleado(int.Parse(txtCiEmpleado.Text), txtNombreEmpleado.Text, dateFechaIngresoEmpleado.Value, txtRefugio.Text,txtPassword.Text);
            if (aux==true)
            {
                MessageBox.Show("El empleado ha sido creado con exito");
            }
            else
            {
                MessageBox.Show("El empleado no ha sido creado");
            }
        }

        private void btnModificarEmpleado_Click(object sender, EventArgs e)
        {
            bool aux = AccesoControllerEmpleado.getInstancia().modificarEmpleado(int.Parse(txtIdEmpleado.Text),int.Parse(txtCiEmpleado.Text), txtNombreEmpleado.Text, dateFechaIngresoEmpleado.Value, txtRefugio.Text,(sControllerEmpleado.Estado)Enum.Parse(typeof(sControllerEmpleado.Estado),ddlEstadoEmpleado.SelectedValue.ToString()),txtPassword.Text);
            if (aux == true)
            {
                MessageBox.Show("El empleado ha sido creado con exito");
            }
            else
            {
                MessageBox.Show("El empleado no ha sido creado");
            }
        }


    }
}
