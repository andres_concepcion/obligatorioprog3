﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppEscritorio
{
    class AccesoControllerEmpleado
    {
        #region Singleton
        private static AccesoControllerEmpleado instancia;
        private AccesoControllerEmpleado() { svc = new sControllerEmpleado.ControllerEmpleadoClient(); }
        public static AccesoControllerEmpleado getInstancia()
        {
            if (instancia == null)
            {
                instancia = new AccesoControllerEmpleado();
            }
            return instancia;
        }
        #endregion
        private static sControllerEmpleado.ControllerEmpleadoClient svc;
        public bool altaEmpleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, string pPassword)
        {
            return svc.altaEmpleado(pCi, pNombre, pFechaingreso, pRefugio, pPassword);
        }
        public bool modificarEmpleado(int pId, int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, sControllerEmpleado.Estado pEstado, string pPassword)
        {
            return svc.modificarEmpleado(pId, pCi, pNombre, pFechaingreso, pRefugio, pEstado, pPassword);
        }

        public sControllerEmpleado.Empleado buscarEmpleadoPorId(int pId)
        {
            return svc.buscarEmpleadoPorId(pId);
        }
        public string inicioSesion(int pUsuario, string pPassword)
        {
            return svc.inicioSesion(pUsuario, pPassword);
        }

    }
}
