﻿namespace AppEscritorio
{
    partial class GestionONG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEmpleados = new System.Windows.Forms.Panel();
            this.txtRefugio = new System.Windows.Forms.TextBox();
            this.lblRefugio = new System.Windows.Forms.Label();
            this.btnModificarEmpleado = new System.Windows.Forms.Button();
            this.btnBuscarEmpleado = new System.Windows.Forms.Button();
            this.lblEstadoEmpleado = new System.Windows.Forms.Label();
            this.ddlEstadoEmpleado = new System.Windows.Forms.ComboBox();
            this.btnAgregarEmpleado = new System.Windows.Forms.Button();
            this.txtNombreEmpleado = new System.Windows.Forms.TextBox();
            this.txtIdEmpleado = new System.Windows.Forms.TextBox();
            this.txtCiEmpleado = new System.Windows.Forms.TextBox();
            this.dateFechaIngresoEmpleado = new System.Windows.Forms.DateTimePicker();
            this.lblFechaIngresoEmpleado = new System.Windows.Forms.Label();
            this.lblNombreEmpleado = new System.Windows.Forms.Label();
            this.lblCiEmpleado = new System.Windows.Forms.Label();
            this.lblIdEmpleado = new System.Windows.Forms.Label();
            this.btnEmpleado = new System.Windows.Forms.Button();
            this.lblUsuarioLogin = new System.Windows.Forms.Label();
            this.lblPasswordLogin = new System.Windows.Forms.Label();
            this.txtUsuarioLogin = new System.Windows.Forms.TextBox();
            this.txtPasswordLogin = new System.Windows.Forms.TextBox();
            this.btnEntrar = new System.Windows.Forms.Button();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.panelEmpleados.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEmpleados
            // 
            this.panelEmpleados.Controls.Add(this.txtPassword);
            this.panelEmpleados.Controls.Add(this.lblPassword);
            this.panelEmpleados.Controls.Add(this.txtRefugio);
            this.panelEmpleados.Controls.Add(this.lblRefugio);
            this.panelEmpleados.Controls.Add(this.btnModificarEmpleado);
            this.panelEmpleados.Controls.Add(this.btnBuscarEmpleado);
            this.panelEmpleados.Controls.Add(this.lblEstadoEmpleado);
            this.panelEmpleados.Controls.Add(this.ddlEstadoEmpleado);
            this.panelEmpleados.Controls.Add(this.btnAgregarEmpleado);
            this.panelEmpleados.Controls.Add(this.txtNombreEmpleado);
            this.panelEmpleados.Controls.Add(this.txtIdEmpleado);
            this.panelEmpleados.Controls.Add(this.txtCiEmpleado);
            this.panelEmpleados.Controls.Add(this.dateFechaIngresoEmpleado);
            this.panelEmpleados.Controls.Add(this.lblFechaIngresoEmpleado);
            this.panelEmpleados.Controls.Add(this.lblNombreEmpleado);
            this.panelEmpleados.Controls.Add(this.lblCiEmpleado);
            this.panelEmpleados.Controls.Add(this.lblIdEmpleado);
            this.panelEmpleados.Location = new System.Drawing.Point(14, 42);
            this.panelEmpleados.Name = "panelEmpleados";
            this.panelEmpleados.Size = new System.Drawing.Size(327, 278);
            this.panelEmpleados.TabIndex = 0;
            this.panelEmpleados.Visible = false;
            // 
            // txtRefugio
            // 
            this.txtRefugio.Location = new System.Drawing.Point(106, 151);
            this.txtRefugio.Name = "txtRefugio";
            this.txtRefugio.Size = new System.Drawing.Size(209, 20);
            this.txtRefugio.TabIndex = 14;
            // 
            // lblRefugio
            // 
            this.lblRefugio.AutoSize = true;
            this.lblRefugio.Location = new System.Drawing.Point(17, 151);
            this.lblRefugio.Name = "lblRefugio";
            this.lblRefugio.Size = new System.Drawing.Size(44, 13);
            this.lblRefugio.TabIndex = 13;
            this.lblRefugio.Text = "Refugio";
            // 
            // btnModificarEmpleado
            // 
            this.btnModificarEmpleado.Location = new System.Drawing.Point(93, 230);
            this.btnModificarEmpleado.Name = "btnModificarEmpleado";
            this.btnModificarEmpleado.Size = new System.Drawing.Size(75, 23);
            this.btnModificarEmpleado.TabIndex = 12;
            this.btnModificarEmpleado.Text = "Modificar";
            this.btnModificarEmpleado.UseVisualStyleBackColor = true;
            this.btnModificarEmpleado.Click += new System.EventHandler(this.btnModificarEmpleado_Click);
            // 
            // btnBuscarEmpleado
            // 
            this.btnBuscarEmpleado.Location = new System.Drawing.Point(243, 6);
            this.btnBuscarEmpleado.Name = "btnBuscarEmpleado";
            this.btnBuscarEmpleado.Size = new System.Drawing.Size(75, 23);
            this.btnBuscarEmpleado.TabIndex = 11;
            this.btnBuscarEmpleado.Text = "Buscar";
            this.btnBuscarEmpleado.UseVisualStyleBackColor = true;
            this.btnBuscarEmpleado.Click += new System.EventHandler(this.btnBuscarEmpleado_Click);
            // 
            // lblEstadoEmpleado
            // 
            this.lblEstadoEmpleado.AutoSize = true;
            this.lblEstadoEmpleado.Location = new System.Drawing.Point(13, 206);
            this.lblEstadoEmpleado.Name = "lblEstadoEmpleado";
            this.lblEstadoEmpleado.Size = new System.Drawing.Size(40, 13);
            this.lblEstadoEmpleado.TabIndex = 10;
            this.lblEstadoEmpleado.Text = "Estado";
            // 
            // ddlEstadoEmpleado
            // 
            this.ddlEstadoEmpleado.FormattingEnabled = true;
            this.ddlEstadoEmpleado.Items.AddRange(new object[] {
            "ACTIVO",
            "BAJA"});
            this.ddlEstadoEmpleado.Location = new System.Drawing.Point(106, 203);
            this.ddlEstadoEmpleado.Name = "ddlEstadoEmpleado";
            this.ddlEstadoEmpleado.Size = new System.Drawing.Size(209, 21);
            this.ddlEstadoEmpleado.TabIndex = 9;
            // 
            // btnAgregarEmpleado
            // 
            this.btnAgregarEmpleado.Location = new System.Drawing.Point(12, 230);
            this.btnAgregarEmpleado.Name = "btnAgregarEmpleado";
            this.btnAgregarEmpleado.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarEmpleado.TabIndex = 8;
            this.btnAgregarEmpleado.Text = "Agregar";
            this.btnAgregarEmpleado.UseVisualStyleBackColor = true;
            this.btnAgregarEmpleado.Click += new System.EventHandler(this.btnAgregarEmpleado_Click);
            // 
            // txtNombreEmpleado
            // 
            this.txtNombreEmpleado.Location = new System.Drawing.Point(106, 79);
            this.txtNombreEmpleado.Name = "txtNombreEmpleado";
            this.txtNombreEmpleado.Size = new System.Drawing.Size(209, 20);
            this.txtNombreEmpleado.TabIndex = 7;
            // 
            // txtIdEmpleado
            // 
            this.txtIdEmpleado.Location = new System.Drawing.Point(106, 8);
            this.txtIdEmpleado.Name = "txtIdEmpleado";
            this.txtIdEmpleado.Size = new System.Drawing.Size(128, 20);
            this.txtIdEmpleado.TabIndex = 6;
            // 
            // txtCiEmpleado
            // 
            this.txtCiEmpleado.Location = new System.Drawing.Point(106, 43);
            this.txtCiEmpleado.Name = "txtCiEmpleado";
            this.txtCiEmpleado.Size = new System.Drawing.Size(209, 20);
            this.txtCiEmpleado.TabIndex = 5;
            // 
            // dateFechaIngresoEmpleado
            // 
            this.dateFechaIngresoEmpleado.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateFechaIngresoEmpleado.Location = new System.Drawing.Point(106, 118);
            this.dateFechaIngresoEmpleado.Name = "dateFechaIngresoEmpleado";
            this.dateFechaIngresoEmpleado.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dateFechaIngresoEmpleado.Size = new System.Drawing.Size(209, 20);
            this.dateFechaIngresoEmpleado.TabIndex = 4;
            // 
            // lblFechaIngresoEmpleado
            // 
            this.lblFechaIngresoEmpleado.AutoSize = true;
            this.lblFechaIngresoEmpleado.Location = new System.Drawing.Point(11, 118);
            this.lblFechaIngresoEmpleado.Name = "lblFechaIngresoEmpleado";
            this.lblFechaIngresoEmpleado.Size = new System.Drawing.Size(89, 13);
            this.lblFechaIngresoEmpleado.TabIndex = 3;
            this.lblFechaIngresoEmpleado.Text = "Fecha de ingreso";
            // 
            // lblNombreEmpleado
            // 
            this.lblNombreEmpleado.AutoSize = true;
            this.lblNombreEmpleado.Location = new System.Drawing.Point(13, 79);
            this.lblNombreEmpleado.Name = "lblNombreEmpleado";
            this.lblNombreEmpleado.Size = new System.Drawing.Size(44, 13);
            this.lblNombreEmpleado.TabIndex = 2;
            this.lblNombreEmpleado.Text = "Nombre";
            // 
            // lblCiEmpleado
            // 
            this.lblCiEmpleado.AutoSize = true;
            this.lblCiEmpleado.Location = new System.Drawing.Point(13, 43);
            this.lblCiEmpleado.Name = "lblCiEmpleado";
            this.lblCiEmpleado.Size = new System.Drawing.Size(23, 13);
            this.lblCiEmpleado.TabIndex = 1;
            this.lblCiEmpleado.Text = "C.I.";
            // 
            // lblIdEmpleado
            // 
            this.lblIdEmpleado.AutoSize = true;
            this.lblIdEmpleado.Location = new System.Drawing.Point(13, 11);
            this.lblIdEmpleado.Name = "lblIdEmpleado";
            this.lblIdEmpleado.Size = new System.Drawing.Size(18, 13);
            this.lblIdEmpleado.TabIndex = 0;
            this.lblIdEmpleado.Text = "ID";
            // 
            // btnEmpleado
            // 
            this.btnEmpleado.Location = new System.Drawing.Point(12, 13);
            this.btnEmpleado.Name = "btnEmpleado";
            this.btnEmpleado.Size = new System.Drawing.Size(75, 23);
            this.btnEmpleado.TabIndex = 1;
            this.btnEmpleado.Text = "Empleados";
            this.btnEmpleado.UseVisualStyleBackColor = true;
            this.btnEmpleado.Visible = false;
            this.btnEmpleado.Click += new System.EventHandler(this.btnEmpleado_Click);
            // 
            // lblUsuarioLogin
            // 
            this.lblUsuarioLogin.AutoSize = true;
            this.lblUsuarioLogin.Location = new System.Drawing.Point(91, 95);
            this.lblUsuarioLogin.Name = "lblUsuarioLogin";
            this.lblUsuarioLogin.Size = new System.Drawing.Size(43, 13);
            this.lblUsuarioLogin.TabIndex = 14;
            this.lblUsuarioLogin.Text = "Usuario";
            // 
            // lblPasswordLogin
            // 
            this.lblPasswordLogin.AutoSize = true;
            this.lblPasswordLogin.Location = new System.Drawing.Point(77, 121);
            this.lblPasswordLogin.Name = "lblPasswordLogin";
            this.lblPasswordLogin.Size = new System.Drawing.Size(53, 13);
            this.lblPasswordLogin.TabIndex = 15;
            this.lblPasswordLogin.Text = "Password";
            // 
            // txtUsuarioLogin
            // 
            this.txtUsuarioLogin.Location = new System.Drawing.Point(136, 92);
            this.txtUsuarioLogin.Name = "txtUsuarioLogin";
            this.txtUsuarioLogin.Size = new System.Drawing.Size(128, 20);
            this.txtUsuarioLogin.TabIndex = 16;
            // 
            // txtPasswordLogin
            // 
            this.txtPasswordLogin.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.txtPasswordLogin.Location = new System.Drawing.Point(136, 118);
            this.txtPasswordLogin.Name = "txtPasswordLogin";
            this.txtPasswordLogin.PasswordChar = '*';
            this.txtPasswordLogin.Size = new System.Drawing.Size(128, 20);
            this.txtPasswordLogin.TabIndex = 17;
            // 
            // btnEntrar
            // 
            this.btnEntrar.Location = new System.Drawing.Point(120, 144);
            this.btnEntrar.Name = "btnEntrar";
            this.btnEntrar.Size = new System.Drawing.Size(75, 23);
            this.btnEntrar.TabIndex = 18;
            this.btnEntrar.Text = "ENTRAR";
            this.btnEntrar.UseVisualStyleBackColor = true;
            this.btnEntrar.Click += new System.EventHandler(this.btnEntrar_Click);
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(106, 177);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(209, 20);
            this.txtPassword.TabIndex = 16;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(17, 177);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 13);
            this.lblPassword.TabIndex = 15;
            this.lblPassword.Text = "Password";
            // 
            // GestionONG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 329);
            this.Controls.Add(this.btnEntrar);
            this.Controls.Add(this.txtPasswordLogin);
            this.Controls.Add(this.txtUsuarioLogin);
            this.Controls.Add(this.lblPasswordLogin);
            this.Controls.Add(this.lblUsuarioLogin);
            this.Controls.Add(this.btnEmpleado);
            this.Controls.Add(this.panelEmpleados);
            this.MaximizeBox = false;
            this.Name = "GestionONG";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestion ONG";
            this.panelEmpleados.ResumeLayout(false);
            this.panelEmpleados.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelEmpleados;
        private System.Windows.Forms.Button btnEmpleado;
        private System.Windows.Forms.Label lblEstadoEmpleado;
        private System.Windows.Forms.ComboBox ddlEstadoEmpleado;
        private System.Windows.Forms.Button btnAgregarEmpleado;
        private System.Windows.Forms.TextBox txtNombreEmpleado;
        private System.Windows.Forms.TextBox txtIdEmpleado;
        private System.Windows.Forms.TextBox txtCiEmpleado;
        private System.Windows.Forms.DateTimePicker dateFechaIngresoEmpleado;
        private System.Windows.Forms.Label lblFechaIngresoEmpleado;
        private System.Windows.Forms.Label lblNombreEmpleado;
        private System.Windows.Forms.Label lblCiEmpleado;
        private System.Windows.Forms.Label lblIdEmpleado;
        private System.Windows.Forms.Button btnModificarEmpleado;
        private System.Windows.Forms.Button btnBuscarEmpleado;
        private System.Windows.Forms.Label lblUsuarioLogin;
        private System.Windows.Forms.Label lblPasswordLogin;
        private System.Windows.Forms.TextBox txtUsuarioLogin;
        private System.Windows.Forms.TextBox txtPasswordLogin;
        private System.Windows.Forms.Button btnEntrar;
        private System.Windows.Forms.TextBox txtRefugio;
        private System.Windows.Forms.Label lblRefugio;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblPassword;
    }
}

