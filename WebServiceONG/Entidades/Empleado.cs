﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace WebServiceONG.Entidades
{
    public enum Estado
    {
        ACTIVO = 0,
        BAJA = 1
    }
    [DataContract]
    public class Empleado
    {
        private static int _ultimoId = 1;
        #region Atributos
        private int _id;
        private int _ci;
        private string _nombre;
        private DateTime _fechaIngreso;
        private string _refugio;
        private Estado _estado;
        #endregion
        #region Propiedades
        [DataMember]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [DataMember]
        public int Ci
        {
            get { return _ci; }
            set { _ci = value; }
        }
        [DataMember]
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        [DataMember]
        public DateTime FechaIngreso
        {
            get { return _fechaIngreso; }
            set { _fechaIngreso = value; }
        }
        [DataMember]
        public string Refugio
        {
            get { return _refugio; }
            set { _refugio = value; }
        }
        [DataMember]
        public Estado Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        #endregion
        #region Constructores
        public Empleado() { }
        public Empleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio)
        {
            Id = _ultimoId;
            Ci = pCi;
            Nombre = pNombre;
            FechaIngreso = pFechaingreso;
            Refugio = pRefugio;
            Estado = Estado.ACTIVO;
            _ultimoId++;
        }
        #endregion
        #region Metodos
        public void altaEmpleado()
        {
            PersistirEmpleado.getInstancia().altaEmpleado(this);
        }
        public void modificarEmpleado()
        {
            PersistirEmpleado.getInstancia().modificarEmpleado(this);
        }
        public Empleado buscarEmpleadoPorId(int pId)
        {
            return PersistirEmpleado.getInstancia().buscarEmpleadoPorId(pId);
        }
        #endregion
    }
}