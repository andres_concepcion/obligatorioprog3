﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebServiceONG.Entidades
{
    public class PersistirEmpleado : Persistir
    {
        #region Singleton
        private static PersistirEmpleado instancia;
        public static PersistirEmpleado getInstancia()
        {
            if (instancia == null)
            {
                instancia = new PersistirEmpleado();
            }
            return instancia;
        }
        private PersistirEmpleado() { }
        #endregion
        public void altaEmpleado(Empleado pEmpleado)
        {
            SqlConnection con = conectar();
            con.Open();
            SqlTransaction trans = con.BeginTransaction();
            SqlCommand comm = con.CreateCommand();
            comm.Transaction = trans;

            try
            {

                string sql1 = "Insert into Personas (ci, nombre, estado) values(" + pEmpleado.Ci + ", '" + pEmpleado.Nombre + "', '" + pEmpleado.Estado + "');";
                string sql2 = "Insert into Empleados (ciPersonas, fechaIngreso, refugio) values(" + pEmpleado.Ci + ", '" + pEmpleado.FechaIngreso + "', '" + pEmpleado.Refugio + "')";
                comm.CommandText = sql1;
                comm.ExecuteNonQuery();

                comm.CommandText = sql2;
                comm.ExecuteNonQuery();

                trans.Commit();
                
            }
            catch (Exception)
            {
                try
                {
                    trans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (trans.Connection != null)
                    {
                        throw ex;
                    }
                }
            }
            finally
            {
                con.Close();
            }
        }
        public bool modificarEmpleado(Empleado pEmpleado)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Empleados set ciPersonas = " + pEmpleado.Ci + ", nombre = " + pEmpleado.Nombre + ", fechaIngreso = " + pEmpleado.FechaIngreso + ", refugio = " + pEmpleado.Refugio + ", estado = " + pEmpleado.Estado + " where ciPersonas = " + pEmpleado.Id;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Empleado> listarEmpleados()
        {
            try
            {
                List<Empleado> lstAux = new List<Empleado>();
                string sql = "Select * from Empleados";
                SqlDataReader unDr = ejecutarReader(sql);
                Empleado aux = new Empleado();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        
        public Empleado buscarEmpleadoPorId(int pId)
        {
            try
            {
                Empleado aux = new Empleado();
                string sql = "Select * from Empleados where id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public Empleado buscarEmpleadoPorCi(int pCi)
        {
            try
            {
                Empleado aux = new Empleado();
                string sql = "Select * from Empleados where ciPersonas = " + pCi;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public string inicioSesion(int pUsuario, string pPassword)
        {
            try
            {
                string aux = "";
                SqlConnection con = conectar();
                con.Open();
                string sql = "Select Rol from Usuarios where Usuario = " + pUsuario + " and Password = '" + pPassword + "'";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader unDr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (unDr.Read())
                {
                    aux = (string)unDr.GetValue(0);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void registroUsuario(int pUsuario, string pPassword)
        {
            try
            {
                string sql = "Insert into Usuarios (Usuario, Password, Rol) values(" + pUsuario + ", '" + pPassword + "', 'Empleado' )";
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void cambiarContrasenia(int pUsuario, string pPassword)
        {
            try
            {
                string sql = "Update Usuarios set Password = '" + pPassword +"' where Usuario = " + pUsuario;
                ejecutarNonQuery(sql);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}