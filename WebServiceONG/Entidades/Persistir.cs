﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebServiceONG.Entidades
{
    public class Persistir
    {
        public SqlConnection conectar()
        {
            try
            {
                string ConnectionString = @"Data Source = THEWITCHER\SQLEXPRESS; Initial Catalog = Ongs; Integrated Security = True";
                SqlConnection con = new SqlConnection(ConnectionString);
                return con;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public int ejecutarNonQuery(string sql)
        {
            try
            {
                int res;
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                res = cmd.ExecuteNonQuery();
                con.Close();
                return res;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public object ejecutarScalar(string sql)
        {
            try
            {
                object res = null;
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                res = cmd.ExecuteScalar();
                con.Close();
                return res;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public SqlDataReader ejecutarReader(string sql)
        {
            try
            {
                SqlConnection con = conectar();
                con.Open();
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader unDr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return unDr;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public int devolverBit(bool b)
        {
            int res = 0;
            if (b == true)
            {
                res = 1;
            }
            return res;
        }
    }
}