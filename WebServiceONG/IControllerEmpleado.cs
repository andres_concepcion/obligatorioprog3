﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WebServiceONG.Entidades;

namespace WebServiceONG
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IControllerEmpleado
    {
        [OperationContract]
        bool altaEmpleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, string pPassword);
        [OperationContract]
        bool modificarEmpleado(int pId, int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, Estado pEstado,string pPassword);
        [OperationContract]
        Empleado buscarEmpleadoPorId(int pId);
        [OperationContract]
        string inicioSesion(int pUsuario, string pPassword);
    }
}
