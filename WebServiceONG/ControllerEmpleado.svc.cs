﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WebServiceONG.Entidades;

namespace WebServiceONG
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ControllerEmpleado : IControllerEmpleado
    {
        public bool altaEmpleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, string pPassword)
        {
            bool control = false;
            Empleado emp = PersistirEmpleado.getInstancia().buscarEmpleadoPorCi(pCi);
            if (emp != null)
            {
                emp = new Empleado(pCi, pNombre, pFechaingreso, pRefugio);
                emp.altaEmpleado();
                PersistirEmpleado.getInstancia().registroUsuario(emp.Id, pPassword);
            }
            return control;
        }
        public bool modificarEmpleado(int pId, int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, Estado pEstado, string pPassword)
        {
            bool control = false;
            Empleado emp = PersistirEmpleado.getInstancia().buscarEmpleadoPorId(pId);
            if (emp != null)
            {
                emp.Ci = pCi;
                emp.Nombre = pNombre;
                emp.FechaIngreso = pFechaingreso;
                emp.Refugio = pRefugio;
                emp.Estado = pEstado;
                emp.modificarEmpleado();
                PersistirEmpleado.getInstancia().cambiarContrasenia(emp.Id, pPassword);
                control = true;
            }
            return control;
        }
        public Empleado buscarEmpleadoPorId(int pId)
        {
            return PersistirEmpleado.getInstancia().buscarEmpleadoPorId(pId);
        }

        public string inicioSesion(int pUsuario, string pPassword)
        {
            return PersistirEmpleado.getInstancia().inicioSesion(pUsuario, pPassword);
        }
    }
}
