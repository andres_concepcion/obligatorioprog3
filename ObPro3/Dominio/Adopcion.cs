﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Adopcion
    {
        public enum EstadoAdopcion
        {
            INICIADA = 0,
            EN_PROCESO = 1,
            COMPLETADA = 2,
            RECHAZADA = 3

        }
        #region Atributos
        private int _id;
        private DateTime _fechaInicio;
        private EstadoAdopcion _estado;
        private Animal _animal;
        private Padrino _padrino;
        private Empleado _empleado;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public DateTime FechaInicio
        {
            get { return _fechaInicio; }
            set { _fechaInicio = value; }
        }
        public EstadoAdopcion Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        public Animal Animal
        {
            get { return _animal; }
            set { _animal = value; }
        }
        public Padrino Padrino
        {
            get { return _padrino; }
            set { _padrino = value; }
        }
        public Empleado Empleado
        {
            get { return _empleado; }
            set { _empleado = value; }
        }
        #endregion
        #region Constructores
        public Adopcion() { }
        public Adopcion(DateTime pFechaInicio, Animal pAnimal, Padrino pPadrino, Empleado pEmpleado)
        {
            FechaInicio = pFechaInicio;
            Estado = EstadoAdopcion.INICIADA;
            Animal = pAnimal;
            Padrino = pPadrino;
            Empleado = pEmpleado;
            
        }
        #endregion
        #region Metodos
        public void altaAdopcion()
        {
            ControllerAdopcion.getInstancia().altaAdopcion(this);
        }
        public void modificarAdopcion()
        {
            ControllerAdopcion.getInstancia().modificarAdopcion(this);
        }
        public Adopcion buscarAdopcionPorId(int pId)
        {
            return ControllerAdopcion.getInstancia().buscarAdopcionPorId(pId);
        }
        #endregion
    }
}