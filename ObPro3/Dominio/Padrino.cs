﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Padrino : Persona
    {
        #region Atributos
        private int _telefono;
        private string _email;
        private string _direccion;
        #endregion
        #region Propiedades
        public int Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        #endregion
        #region Constructores
        public Padrino() { }
        public Padrino(int pCi, string pNombre, int pTelefono, string pEmail, string pDireccion)
        : base(pCi, pNombre)
        {
            Ci = pCi;
            Nombre = pNombre;
            Telefono = pTelefono;
            Email = pEmail;
            Direccion = pDireccion;
        }
        #endregion
        #region Metodos
        public void altaPadrino()
        {
            ControllerPadrino.getInstancia().altaPadrino(this);
        }
        public void modificarPadrino()
        {
            ControllerPadrino.getInstancia().modificarPadrino(this);
        }
        public Padrino buscarPadrinoPorCi(int pCi)
        {
            return ControllerPadrino.getInstancia().buscarPadrinoPorCi(pCi);
        }
        #endregion
    }
}