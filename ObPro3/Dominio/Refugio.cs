﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Refugio
    {
        #region Atributos
        private string _nombre;
        private string _direccion;
        private string _zona;
        private List<Animal> _lstAnimales;
        private List<Empleado> _lstEmpleados;
        #endregion
        #region Propiedades
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public string Direccion
        {
            get { return _direccion; }
            set { _direccion = value; }
        }
        public string Zona
        {
            get { return _zona; }
            set { _zona = value; }
        }
        public List<Animal> LstAnimales
        {
            get { return _lstAnimales; }
            set { _lstAnimales = value; }
        }
        public List<Empleado> LstEmpleados
        {
            get { return _lstEmpleados; }
            set { _lstEmpleados = value; }
        }
        #endregion
        #region Constructores
        public Refugio() { }
        public Refugio(string pNombre, string pDireccion, string pZona)
        {
            Nombre = pNombre;
            Direccion = pDireccion;
            Zona = pZona;
            LstAnimales = new List<Animal>();
            LstEmpleados = new List<Empleado>();
        }
        #endregion
        #region Metodos
        public void altaRefugio()
        {
            ControllerRefugio.getInstancia().altaRefugio(this);
        }
        public void modificarRefugio()
        {
            ControllerRefugio.getInstancia().modificarRefugio(this);
        }
        public Refugio buscarRefugioPorNombre(string pNombre)
        {
            return ControllerRefugio.getInstancia().buscarRefugioPorNombre(pNombre);
        }
        #endregion
    }
}