﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Empleado : Persona
    {
        #region Atributos
        private int _id;
        private DateTime _fechaIngreso;
        private string _refugio;
        #endregion
        #region Propiedades
        public string Refugio
        {
            get { return _refugio; }
            set { _refugio = value; }
        }
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public DateTime FechaIngreso
        {
            get { return _fechaIngreso; }
            set { _fechaIngreso = value; }
        }
        #endregion
        #region Constructores
        public Empleado() { }
        public Empleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio)
            : base(pCi, pNombre)
        {
            Ci = pCi;
            Nombre = pNombre;
            FechaIngreso = pFechaingreso;
            Refugio = pRefugio;
            Estado = Estado.ACTIVO;
        }
        #endregion
        #region Metodos
        public void altaEmpleado()
        {
            ControllerEmpleado.getInstancia().altaEmpleado(this);
        }
        public void modificarEmpleado()
        {
            ControllerEmpleado.getInstancia().modificarEmpleado(this);
        }
        public Empleado buscarEmpleadoPorId(int pId)
        {
            return ControllerEmpleado.getInstancia().buscarEmpleadoPorId(pId);
        }
        #endregion
    }
}