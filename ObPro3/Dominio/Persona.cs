﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObPro3.Dominio
{
    public enum Estado
    {
        ACTIVO = 0,
        BAJA = 1
    }
    public class Persona
    {
        #region Atributos
        private int _ci;
        private string _nombre;
        private Estado _estado;
        #endregion
        #region Propiedades
        public int Ci
        {
            get { return _ci; }
            set { _ci = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public Estado Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        #endregion
        #region Constructores
        public Persona() { }
        public Persona(int pCi, string pNombre)
        {
            Ci = pCi;
            Nombre = pNombre;
        }
        #endregion
    }
}