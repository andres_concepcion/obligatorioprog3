﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Animal
    {
        #region Atributos
        private int _id;
        private string _nombre;
        private string _especie;
        private int _tamanio;
        private int _peso;
        private decimal _edad;
        private bool _vacunas;
        private bool _castracion;
        private string _estadoSanitario;
        private DateTime? _fechaDeceso;
        private string _refugio;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }
        public string Especie
        {
            get { return _especie; }
            set { _especie = value; }
        }
        public int Tamanio
        {
            get { return _tamanio; }
            set { _tamanio = value; }
        }
        public int Peso
        {
            get { return _peso; }
            set { _peso = value; }
        }
        public decimal Edad
        {
            get { return _edad; }
            set { _edad = value; }
        }
        public bool Vacunas
        {
            get { return _vacunas; }
            set { _vacunas = value; }
        }
        public bool Castracion
        {
            get { return _castracion; }
            set { _castracion = value; }
        }
        public string EstadoSanitario
        {
            get { return _estadoSanitario; }
            set { _estadoSanitario = value; }
        }
        public DateTime? FechaDeceso
        {
            get { return _fechaDeceso; }
            set { _fechaDeceso = value; }
        }
        public string Refugio
        {
            get { return _refugio; }
            set { _refugio = value; }
        }
        #endregion
        #region Constructores
        public Animal() { }
        public Animal(string pNombre, string pEspecie, int pTamanio, int pPeso, decimal pEdad, bool pVacunas, bool pCastracion, string pEstadoSanitario, string pRefugio)
        {
            Nombre = pNombre;
            Especie = pEspecie;
            Tamanio = pTamanio;
            Peso = pPeso;
            Edad = pEdad;
            Vacunas = pVacunas;
            Castracion = pCastracion;
            EstadoSanitario = pEstadoSanitario;
            FechaDeceso = null;
            Refugio = pRefugio;
        }
        #endregion
        #region Metodos
        public void altaAnimal()
        {
            ControllerAnimal.getInstancia().altaAnimal(this);
        }
        public void modificarAnimal()
        {
            ControllerAnimal.getInstancia().modificarAnimal(this);
        }
        public Animal buscarAnimalPorId(int pId)
        {
            return ControllerAnimal.getInstancia().buscarAnimalPorId(pId);
        }
        #endregion
    }
}