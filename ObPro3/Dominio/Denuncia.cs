﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Persistencia;

namespace ObPro3.Dominio
{
    public class Denuncia
    {
        #region Atributos
        private int _id;
        private DateTime _fecha;
        private int _telefono;
        private string _email;
        private string _descripcion;
        private bool _estado;
        #endregion
        #region Propiedades
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
        public int Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        public bool Estado
        {
            get { return _estado; }
            set { _estado = value; }
        }
        #endregion
        #region Constructores
        public Denuncia() { }
        public Denuncia(int pTelefono, string pEmail, string pDescripcion, bool pEstado)
        {
            Telefono = pTelefono;
            Email = pEmail;
            Descripcion = pDescripcion;
            Estado = pEstado;
        }
        #endregion
        #region Metodos
        public void altaDenuncia()
        {
            ControllerDenuncia.getInstancia().altaDenuncia(this);
        }
        public void modificarDenuncia()
        {
            ControllerDenuncia.getInstancia().modificarDenuncia(this);
        }
        public Denuncia buscarDenunciaPorId(int pId)
        {
            return ControllerDenuncia.getInstancia().buscarDenunciaPorId(pId);
        }
        #endregion
    }
}