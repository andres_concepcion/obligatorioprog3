﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ObPro3.Dominio;
using ObPro3.Persistencia;

namespace ObPro3
{
    public class Ong
    {
        #region Singleton
        private static Ong instancia;
        public static Ong getInstancia()
        {
            if (instancia == null)
            {
                instancia = new Ong();
            }
            return instancia;
        }
        private Ong()
        { }
        #endregion
        #region Metodos
        #region Altas
        public void altaAdopcion(DateTime pFechaInicio, Animal pAnimal, Padrino pPadrino, Empleado pEmpleado)
        {
            Adopcion adop = new Adopcion(pFechaInicio, pAnimal, pPadrino, pEmpleado);
            adop.altaAdopcion();
        }
        public void altaAnimal(string pNombre, string pEspecie, int pTamanio, int pPeso, decimal pEdad, bool pVacunas, bool pCastracion, string pEstadoSanitario, string pRefugio)
        {
            //bool control = false;
            //Animal ani = ControllerAnimal.getInstancia().buscarAnimaloPorId(pCi);
            //if (ani != null)
            //{
            Animal ani = new Animal(pNombre, pEspecie, pTamanio, pPeso, pEdad, pVacunas, pCastracion, pEstadoSanitario, pRefugio);
            ani.altaAnimal();
            //}
            //return control;
        }
        public void altaDenuncia(int pTelefono, string pEmail, string pDescripcion, bool pEstado)
        {
            Denuncia den = new Denuncia(pTelefono, pEmail, pDescripcion, pEstado);
            den.altaDenuncia();
        }
        public bool altaEmpleado(int pCi, string pNombre, DateTime pFechaingreso, string pRefugio)
        {
            bool control = false;
            Empleado emp = buscarEmpleadoPorCi(pCi);
            if (emp != null)
            {
                emp = new Empleado(pCi, pNombre, pFechaingreso, pRefugio);
                emp.altaEmpleado();
                control = true;
            }
            return control;
        }
        public bool altaPadrino(int pCi, string pNombre, int pTelefono, string pEmail, string pDireccion)
        {
            bool control = false;
            Padrino pad = buscarPadrinoPorCi(pCi);
            if (pad != null)
            {
                pad = new Padrino(pCi, pNombre, pTelefono, pEmail, pDireccion);
                pad.altaPadrino();
                control = true;
            }
            return control;
        }
        public bool altaRefugio(string pNombre, string pDireccion, string pZona)
        {
            bool control = false;
            Refugio refu = ControllerRefugio.getInstancia().buscarRefugioPorNombre(pNombre);
            if (refu != null)
            {
                refu = new Refugio(pNombre, pDireccion, pZona);
                refu.altaRefugio();
                control = true;
            }
            return control;
        }
        #endregion
        #region Modificaciones
        public void modificarAdopcion(int pId, Adopcion.EstadoAdopcion pEstado, Animal pAnimal, Padrino pPadrino, Empleado pEmpleado)
        {
            Adopcion adop = ControllerAdopcion.getInstancia().buscarAdopcionPorId(pId);
            if (adop != null)
            {
                adop.Estado = pEstado;
                adop.Animal = pAnimal;
                adop.Padrino = pPadrino;
                adop.Empleado = pEmpleado;
                adop.modificarAdopcion();
            }
        }
        public bool modificarAnimal(int pId, string pNombre, string pEspecie, int pTamanio, int pPeso, decimal pEdad, bool pVacunas, bool pCastracion, string pEstadoSanitario, string pRefugio)
        {
            bool control = false;
            Animal ani = ControllerAnimal.getInstancia().buscarAnimalPorId(pId);
            if (ani != null)
            {
                ani.Nombre = pNombre;
                ani.Especie = pEspecie;
                ani.Tamanio = pTamanio;
                ani.Peso = pPeso;
                ani.Edad = pEdad;
                ani.Vacunas = pVacunas;
                ani.Castracion = pCastracion;
                ani.EstadoSanitario = pEstadoSanitario;
                ani.Refugio = pRefugio;
                ani.modificarAnimal();
                control = true;
            }
            return control;
        }
        public bool modificarDenuncia(int pId, DateTime pFecha, int pTelefono, string pEmail, bool pEstado)
        {
            bool control = false;
            Denuncia den = ControllerDenuncia.getInstancia().buscarDenunciaPorId(pId);
            if (den != null)
            {
                den.Fecha = pFecha;
                den.Telefono = pTelefono;
                den.Email = pEmail;
                den.Estado = pEstado;
                den.modificarDenuncia();
                control = true;
            }
            return control;
        }
        public bool modificarEmpleado(int pId, int pCi, string pNombre, DateTime pFechaingreso, string pRefugio, Estado pEstado)
        {
            bool control = false;
            Empleado emp = ControllerEmpleado.getInstancia().buscarEmpleadoPorId(pId);
            if (emp != null)
            {
                emp.Ci = pCi;
                emp.Nombre = pNombre;
                emp.FechaIngreso = pFechaingreso;
                emp.Refugio = pRefugio;
                emp.Estado = pEstado;
                emp.modificarEmpleado();
                control = true;
            }
            return control;
        }
        public bool modificarPadrino(int pCi, string pNombre, int pTelefono, string pEmail, string pDireccion, Estado pEstado)
        {
            bool control = false;
            Padrino pad = ControllerPadrino.getInstancia().buscarPadrinoPorCi(pCi);
            if (pad != null)
            {
                pad.Nombre = pNombre;
                pad.Telefono = pTelefono;
                pad.Email = pEmail;
                pad.Direccion = pDireccion;
                pad.Estado = pEstado;
                pad.modificarPadrino();
                control = true;
            }
            return control;
        }
        public bool modificarRefugio(string pNombre, string pDireccion, string pZona)
        {
            bool control = false;
            Refugio refu = ControllerRefugio.getInstancia().buscarRefugioPorNombre(pNombre);
            if (refu != null)
            {
                refu.Direccion = pDireccion;
                refu.Zona = pZona;
                refu.altaRefugio();
                control = true;
            }
            return control;
        }
        #endregion
        #region Listados

        public List<Animal> listarAnimales()
        {
            return ControllerAnimal.getInstancia().listarAnimales();
        }
        public List<Animal> reporteAnimalesEnAdopcionPorEstado(string pEstado)
        {
            return ControllerAnimal.getInstancia().listarAnimalesEnAdopcionPorEstado(pEstado);
        }
        public List<Refugio> reporteRefugios()
        {
            return ControllerRefugio.getInstancia().listarRefugios();
        }
        public List<Padrino> reporteAdopcionesPorPadrino()
        {
            return ControllerAdopcion.getInstancia().reporteAdopcionesPorPadrino();
        }
        #endregion
        #region Busquedas
        public Padrino buscarPadrinoPorCi(int pCi)
        {
            return ControllerPadrino.getInstancia().buscarPadrinoPorCi(pCi);
        }
        public Empleado buscarEmpleadoPorCi(int pCi)
        {
            return ControllerEmpleado.getInstancia().buscarEmpleadoPorCi(pCi);
        }
        public Empleado buscarEmpleadoPorId(int pId)
        {
            return ControllerEmpleado.getInstancia().buscarEmpleadoPorId(pId);
        }
        public Animal buscarAnimalPorId(int pId)
        {
            return ControllerAnimal.getInstancia().buscarAnimalPorId(pId);
        }
        public List<string> nombreRefugio() 
        {
            return ControllerRefugio.getInstancia().nombreRefugio();
        }
        #endregion
        #region Otros
        public string inicioSesion(int pUsuario, string pPassword)
        {
            string aux = ControllerUsuario.getInstancia().inicioSesion(pUsuario, pPassword);
            return aux;
        }
        public void registroUsuario(int pUsuario, string pPassword, string pRol)
        {
            ControllerUsuario.getInstancia().registroUsuario(pUsuario, pPassword, pRol);
        }
        public void cambiarContrasenia(int pUsuario, string pPassword)
        {
            ControllerUsuario.getInstancia().cambiarContrasenia(pUsuario, pPassword);
        }
        #endregion
        #endregion
    }
}