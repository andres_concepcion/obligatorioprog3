﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerDenuncia : Persistir
    {
        #region Singleton
        private static ControllerDenuncia instancia;
        public static ControllerDenuncia getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerDenuncia();
            }
            return instancia;
        }
        private ControllerDenuncia() { }
        #endregion
        public bool altaDenuncia(Denuncia pDenuncia)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Denuncias (fecha, telefono, email, descripcion ,estado) values(GETDATE(), " + pDenuncia.Telefono + ", '" + pDenuncia.Email + "', '" + pDenuncia.Descripcion + "'," + devolverBit(pDenuncia.Estado) + ");";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool modificarDenuncia(Denuncia pDenuncia)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Denuncias set fecha = " + pDenuncia.Fecha + ", telefono = " + pDenuncia.Telefono + ", email = " + pDenuncia.Email + ", denuncia = " + pDenuncia.Descripcion + ", estado = " + devolverBit(pDenuncia.Estado) + " where id = " + pDenuncia.Id;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Denuncia> listarDenuncias()
        {
            try
            {
                List<Denuncia> lstAux = new List<Denuncia>();
                string sql = "Select * from Denuncias";
                SqlDataReader unDr = ejecutarReader(sql);
                Denuncia aux = new Denuncia();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Fecha = (DateTime)unDr.GetValue(1);
                    aux.Telefono = (int)unDr.GetValue(2);
                    aux.Email = (string)unDr.GetValue(3);
                    aux.Descripcion = (string)unDr.GetValue(4);
                    if ((int)unDr.GetValue(5) == 1)
                        aux.Estado = true;
                    else
                        aux.Estado = false;
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Denuncia buscarDenunciaPorId(int pId)
        {
            try
            {
                Denuncia aux = new Denuncia();
                string sql = "Select * from Denuncias where id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Fecha = (DateTime)unDr.GetValue(1);
                    aux.Telefono = (int)unDr.GetValue(2);
                    aux.Email = (string)unDr.GetValue(3);
                    aux.Descripcion = (string)unDr.GetValue(4);
                    if ((int)unDr.GetValue(5) == 1)
                        aux.Estado = true;
                    else
                        aux.Estado = false;
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}