﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerAnimal : Persistir
    {
        #region Singleton
        private static ControllerAnimal instancia;
        public static ControllerAnimal getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerAnimal();
            }
            return instancia;
        }
        private ControllerAnimal() { }
        #endregion
        public bool altaAnimal(Animal pAnimal)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Animales (nombre, especie, tamanio, peso, edad, vacunas, castracion, estadoSanitario, refugio) values('" + pAnimal.Nombre + "', '" + pAnimal.Especie + "', " + pAnimal.Tamanio + ", " + pAnimal.Peso + ", " + pAnimal.Edad + ", " + devolverBit(pAnimal.Vacunas) + ", " + devolverBit(pAnimal.Castracion) + ", '" + pAnimal.EstadoSanitario + "', '" + pAnimal.Refugio + "');";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool modificarAnimal(Animal pAnimal)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Animales set nombre = '" + pAnimal.Nombre + "', especie = '" + pAnimal.Especie + "', tamanio = " + pAnimal.Tamanio + ", peso = " + pAnimal.Peso + ", edad = " + pAnimal.Edad + ", vacunas = " + devolverBit(pAnimal.Vacunas) + ", castracion = " + devolverBit(pAnimal.Castracion) + ", estadoSaniario = '" + pAnimal.EstadoSanitario + "', fechaDeceso = '" + pAnimal.FechaDeceso + "', refugio = '" + pAnimal.Refugio + "' where id = " + pAnimal.Id + ";";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Animal> listarAnimales()
        {
            try
            {
                List<Animal> lstAux = new List<Animal>();
                string sql = "Select * from Animales";
                SqlDataReader unDr = ejecutarReader(sql);
                
                while (unDr.Read())
                {
                    Animal aux = new Animal();
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Especie = (string)unDr.GetValue(2);
                    aux.Tamanio = (int)unDr.GetValue(3);
                    aux.Peso = (int)unDr.GetValue(4);
                    aux.Edad = (int)unDr.GetValue(5);
                    aux.Vacunas = (bool)unDr.GetValue(6);
                    aux.Castracion = (bool)unDr.GetValue(7);
                    aux.EstadoSanitario = (string)unDr.GetValue(8);
                    if (unDr.IsDBNull(9))
                    {
                        aux.FechaDeceso = default(DateTime);
                    }
                    else
                    {
                        aux.FechaDeceso = (DateTime?)unDr.GetValue(9);
                    }
                    
                    aux.Refugio = (string)unDr.GetValue(10);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public List<Animal> listarAnimalesPorRefugio(string pNombre)
        {
            try
            {
                List<Animal> lstAux = new List<Animal>();
                string sql = "Select * from Animales where refugio = '" + pNombre + "';";
                SqlDataReader unDr = ejecutarReader(sql);
                Animal aux = new Animal();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Especie = (string)unDr.GetValue(2);
                    aux.Tamanio = (int)unDr.GetValue(3);
                    aux.Peso = (int)unDr.GetValue(4);
                    aux.Edad = (int)unDr.GetValue(5);
                    aux.Vacunas = (bool)unDr.GetValue(6);
                    aux.Castracion = (bool)unDr.GetValue(7);
                    aux.EstadoSanitario = (string)unDr.GetValue(8);
                    if (unDr.IsDBNull(9))
                    {
                        aux.FechaDeceso = default(DateTime);
                    }
                    else
                    {
                        aux.FechaDeceso = (DateTime?)unDr.GetValue(9);
                    }
                    aux.Refugio = (string)unDr.GetValue(10);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public List<Animal> listarAnimalesEnAdopcionPorEstado(string pEstado)
        {
            try
            {
                List<Animal> lstAux = new List<Animal>();
                string sql = "Select a.id, a.nombre, a.especie, a.tamanio, a.peso, a.edad, a.vacunas, a.castracion, a.estadoSanitario, a.fechaDeceso, a.refugio from Animales as a inner join Adopciones as ad on a.id = ad.idAnimal where ad.estado = '" + pEstado + "';";
                SqlDataReader unDr = ejecutarReader(sql);
                
                while (unDr.Read())
                {
                    Animal aux = new Animal();
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Especie = (string)unDr.GetValue(2);
                    aux.Tamanio = (int)unDr.GetValue(3);
                    aux.Peso = (int)unDr.GetValue(4);
                    aux.Edad = (int)unDr.GetValue(5);
                    aux.Vacunas = (bool)unDr.GetValue(6);
                    aux.Castracion = (bool)unDr.GetValue(7);
                    aux.EstadoSanitario = (string)unDr.GetValue(8);
                    if (unDr.IsDBNull(9))
                    {
                        aux.FechaDeceso = default(DateTime);
                    }
                    else
                    {
                        aux.FechaDeceso = (DateTime?)unDr.GetValue(9);
                    }
                    aux.Refugio = (string)unDr.GetValue(10);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Animal buscarAnimalPorId(int pId)
        {
            try
            {
                Animal aux = new Animal();
                string sql = "Select * from Animales where id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Especie = (string)unDr.GetValue(2);
                    aux.Tamanio = (int)unDr.GetValue(3);
                    aux.Peso = (int)unDr.GetValue(4);
                    aux.Edad = (int)unDr.GetValue(5);
                    aux.Vacunas = (bool)unDr.GetValue(6);
                    aux.Castracion = (bool)unDr.GetValue(7);
                    aux.EstadoSanitario = (string)unDr.GetValue(8);
                    if (unDr.IsDBNull(9))
                    {
                        aux.FechaDeceso = default(DateTime);
                    }
                    else
                    {
                        aux.FechaDeceso = (DateTime?)unDr.GetValue(9);
                    }
                    aux.Refugio = (string)unDr.GetValue(10);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}