﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerAdopcion : Persistir
    {
        #region Singleton
        private static ControllerAdopcion instancia;
        public static ControllerAdopcion getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerAdopcion();
            }
            return instancia;
        }
        private ControllerAdopcion() { }
        #endregion
        public bool altaAdopcion(Adopcion pAdopcion)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Adopciones (fechaIngreso, estado, idAnimal, ciPadrino, idEmpleado) values(GETDATE(), '" + pAdopcion.Estado + "', " + pAdopcion.Animal.Id + ", " + pAdopcion.Padrino.Ci + ", " + pAdopcion.Empleado.Id + ");";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool modificarAdopcion(Adopcion pAdopcion)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Adopciones set fechaInicio = " + pAdopcion.FechaInicio + ", estado = " + pAdopcion.Estado + ", idAnimal = " + pAdopcion.Animal.Id + ", ciPadrino = " + pAdopcion.Padrino.Ci + ", ciEmpleado = " + pAdopcion.Empleado.Id + " where id = " + pAdopcion.Id;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Adopcion> listarAdopcion()
        {
            try
            {
                List<Adopcion> lstAux = new List<Adopcion>();
                string sql = "Select * from Adopciones";
                SqlDataReader unDr = ejecutarReader(sql);
                Adopcion aux = new Adopcion();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.FechaInicio = (DateTime)unDr.GetValue(1);
                    aux.Estado = (Adopcion.EstadoAdopcion)unDr.GetValue(2);
                    aux.Animal = ControllerAnimal.getInstancia().buscarAnimalPorId((int)unDr.GetValue(3));
                    aux.Padrino = ControllerPadrino.getInstancia().buscarPadrinoPorCi((int)unDr.GetValue(4));
                    aux.Empleado = ControllerEmpleado.getInstancia().buscarEmpleadoPorId((int)unDr.GetValue(5));
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<Padrino> reporteAdopcionesPorPadrino()
        {
            try
            {
                List<Padrino> lstAux = new List<Padrino>();
                string sql = "Select count(DISTINCT ciPadrino) as cantidad, ciPadrino from Adopciones where fechaIngreso between DATEADD(year,-2,getdate()) and GETDATE() group by ciPadrino;";
                SqlDataReader unDr = ejecutarReader(sql);
                Padrino aux = new Padrino();
                while (unDr.Read())
                {
                    if ((int)unDr.GetValue(0) > 1)
                    {
                        aux = ControllerPadrino.getInstancia().buscarPadrinoPorCi((int)unDr.GetValue(1));
                        lstAux.Add(aux);
                    }
                }
                return lstAux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public Adopcion buscarAdopcionPorId(int pId)
        {
            try
            {
                string sql = "Select * from Adopciones where id = " + pId;
                SqlDataReader unDr = ejecutarReader(sql);
                Adopcion aux = new Adopcion();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.FechaInicio = (DateTime)unDr.GetValue(1);
                    aux.Estado = (Adopcion.EstadoAdopcion)unDr.GetValue(2);
                    aux.Animal = ControllerAnimal.getInstancia().buscarAnimalPorId((int)unDr.GetValue(3));
                    aux.Padrino = ControllerPadrino.getInstancia().buscarPadrinoPorCi((int)unDr.GetValue(4));
                    aux.Empleado = ControllerEmpleado.getInstancia().buscarEmpleadoPorId((int)unDr.GetValue(5));
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}