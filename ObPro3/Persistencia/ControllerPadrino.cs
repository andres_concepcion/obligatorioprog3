﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerPadrino : Persistir
    {
        #region Singleton
        private static ControllerPadrino instancia;
        public static ControllerPadrino getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerPadrino();
            }
            return instancia;
        }
        private ControllerPadrino() { }
        #endregion
        public void altaPadrino(Padrino pPadrino)
        {
            SqlConnection con = conectar();
            con.Open();
            SqlTransaction trans = con.BeginTransaction();
            SqlCommand comm = con.CreateCommand();
            comm.Transaction = trans;

            try
            {

                string sql1 = "Insert into Personas (ci, nombre, estado) values(" + pPadrino.Ci + ", '" + pPadrino.Nombre + "', '" + pPadrino.Estado + "');";
                string sql2 = "Insert into Padrinos (ciPersona, telefono, email, direccion) values(" + pPadrino.Ci + ", " + pPadrino.Telefono + ", '" + pPadrino.Email + "', '" + pPadrino.Direccion + "')";

                comm.CommandText = sql1;
                comm.ExecuteNonQuery();

                comm.CommandText = sql2;
                comm.ExecuteNonQuery();

                trans.Commit();
            }
            catch (Exception)
            {
                try
                {
                    trans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (trans.Connection != null)
                    {
                        throw ex;
                    }
                }
            }
            finally 
            {
                con.Close();
            }
        }

        public bool modificarPadrino(Padrino pPadrino)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Padrinos set nombre = " + pPadrino.Nombre + ", telefono = " + pPadrino.Telefono + ", email = " + pPadrino.Email + ", direccion = " + pPadrino.Direccion + " where ciPersona = " + pPadrino.Ci;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Padrino> listarPadrinos()
        {
            try
            {
                List<Padrino> lstAux = new List<Padrino>();
                string sql = "Select * from Padrinos";
                SqlDataReader unDr = ejecutarReader(sql);
                Padrino aux = new Padrino();
                while (unDr.Read())
                {
                    aux.Ci = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Telefono = (int)unDr.GetValue(2);
                    aux.Email = (string)unDr.GetValue(3);
                    aux.Direccion = (string)unDr.GetValue(4);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Padrino buscarPadrinoPorCi(int pCi)
        {
            try
            {
                Padrino aux = new Padrino();
                string sql = "Select ci, nombre, telefono, email, direccion  from Personas inner join Padrinos on Personas.ci = Padrinos.ciPersona where ci = " + pCi + ";";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Ci = (int)unDr.GetValue(0);
                    aux.Nombre = (string)unDr.GetValue(1);
                    aux.Telefono = (int)unDr.GetValue(2);
                    aux.Email = (string)unDr.GetValue(3);
                    aux.Direccion = (string)unDr.GetValue(4);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}