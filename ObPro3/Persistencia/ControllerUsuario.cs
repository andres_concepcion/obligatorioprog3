﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace ObPro3.Persistencia
{
    public class ControllerUsuario : Persistir
    {
        #region Singleton
        private static ControllerUsuario instancia;
        public static ControllerUsuario getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerUsuario();
            }
            return instancia;
        }
        private ControllerUsuario() { }
        #endregion
        public string inicioSesion(int pUsuario, string pPassword)
        {
            try
            {
                string sql = "Select Rol from Usuarios where Usuario = " + pUsuario + " and Password = '" + pPassword + "'";
                object aux = ejecutarScalar(sql);
                string auxRol = "";
                if (aux!=null )
                {
                    auxRol = (string)aux;
                }
                return auxRol;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void registroUsuario(int pUsuario, string pPassword, string pRol)
        {
            try
            {
                string sql = "Insert into Usuarios (Usuario, Password, Rol) values(" + pUsuario + ", '" + pPassword + "', '" +pRol + "');";
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void cambiarContrasenia(int pUsuario, string pPassword)
        {
            try
            {
                string sql = "Update Usuarios set Password = " + pPassword + " where Usuario = " + pUsuario;
                int i = ejecutarNonQuery(sql);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}