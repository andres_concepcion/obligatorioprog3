﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerEmpleado : Persistir
    {
        #region Singleton
        private static ControllerEmpleado instancia;
        public static ControllerEmpleado getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerEmpleado();
            }
            return instancia;
        }
        private ControllerEmpleado() { }
        #endregion
        public bool altaEmpleado(Empleado pEmpleado)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Empleados (id, ciPersonas, nombre, fechaIngreso, refugio, estado) values(" + pEmpleado.Id + ", " + pEmpleado.Ci + ", " + pEmpleado.Nombre + ", " + pEmpleado.FechaIngreso + ", " + pEmpleado.Refugio + ", " + pEmpleado.Estado + ")";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool modificarEmpleado(Empleado pEmpleado)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Padrinos set ciPersonas = " + pEmpleado.Ci + ", nombre = " + pEmpleado.Nombre + ", fechaIngreso = " + pEmpleado.FechaIngreso + ", refugio = " + pEmpleado.Refugio + ", estado = " + pEmpleado.Estado + " where id = " + pEmpleado.Id;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Empleado> listarEmpleados()
        {
            try
            {
                List<Empleado> lstAux = new List<Empleado>();
                string sql = "Select * from Empleados";
                SqlDataReader unDr = ejecutarReader(sql);
                Empleado aux = new Empleado();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public List<Empleado> listarEmpleadosPorRefugio(string pNombreRefugio)
        {
            try
            {
                List<Empleado> lstAux = new List<Empleado>();
                string sql = "Select * from Empleados where refugio =" + pNombreRefugio;
                SqlDataReader unDr = ejecutarReader(sql);
                Empleado aux = new Empleado();
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Empleado buscarEmpleadoPorId(int pId)
        {
            try
            {
                Empleado aux = new Empleado();
                string sql = "select id, ci, nombre, fechaIngreso, refugio  from Personas inner join Empleados on Personas.ci = Empleados.ciPersonas where id = " + pId + ";";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public Empleado buscarEmpleadoPorCi(int pCi)
        {
            try
            {
                Empleado aux = new Empleado();
                string sql = "Select * from Empleados where ciPersonas = " + pCi;
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Id = (int)unDr.GetValue(0);
                    aux.Ci = (int)unDr.GetValue(1);
                    aux.Nombre = (string)unDr.GetValue(2);
                    aux.FechaIngreso = (DateTime)unDr.GetValue(3);
                    aux.Refugio = (string)unDr.GetValue(4);
                    aux.Estado = (Estado)unDr.GetValue(5);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}