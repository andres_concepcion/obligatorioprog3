﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using ObPro3.Dominio;

namespace ObPro3.Persistencia
{
    public class ControllerRefugio : Persistir
    {
        #region Singleton
        private static ControllerRefugio instancia;
        public static ControllerRefugio getInstancia()
        {
            if (instancia == null)
            {
                instancia = new ControllerRefugio();
            }
            return instancia;
        }
        private ControllerRefugio() { }
        #endregion

        public bool altaRefugio(Refugio pRefugio)
        {
            try
            {
                bool resultado = false;
                string sql = "Insert into Refugios (nombre, direccion, zona) values('" + pRefugio.Nombre + "', '" + pRefugio.Direccion + "',' " + pRefugio.Zona + "');";
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool modificarRefugio(Refugio pRefugio)
        {
            try
            {
                bool resultado = false;
                string sql = "Update Refugios set direccion = '" + pRefugio.Direccion + "', zona = '" + pRefugio.Zona + "' where nombre = " + pRefugio.Nombre;
                int i = ejecutarNonQuery(sql);
                if (i != 0)
                    resultado = true;
                return resultado;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public List<Refugio> listarRefugios()
        {
            try
            {
                List<Refugio> lstAux = new List<Refugio>();
                string sql = "Select * from Refugios";
                SqlDataReader unDr = ejecutarReader(sql);
                
                while (unDr.Read())
                {
                    Refugio aux = new Refugio();
                    aux.Nombre = (string)unDr.GetValue(0);
                    aux.Direccion = (string)unDr.GetValue(1);
                    aux.Zona = (string)unDr.GetValue(2);
                    //aux.LstAnimales = ControllerAnimal.getInstancia().listarAnimalesPorRefugio(aux.Nombre);
                    //aux.LstEmpleados = ControllerEmpleado.getInstancia().listarEmpleadosPorRefugio(aux.Nombre);
                    lstAux.Add(aux);
                }
                return lstAux;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public Refugio buscarRefugioPorNombre(string pNombre)
        {
            try
            {
                Refugio aux = new Refugio();
                string sql = "Select * from Refugios where nombre = '" + pNombre + "';";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Nombre = (string)unDr.GetValue(0);
                    aux.Direccion = (string)unDr.GetValue(1);
                    aux.Zona = (string)unDr.GetValue(2);
                    aux.LstAnimales = ControllerAnimal.getInstancia().listarAnimalesPorRefugio(aux.Nombre);
                    aux.LstEmpleados = ControllerEmpleado.getInstancia().listarEmpleadosPorRefugio(aux.Nombre);
                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string buscarNombreRefugio(string pNombre)
        {
            try
            {
                Refugio aux = new Refugio();
                string sql = "Select * from Refugios where nombre = '" + pNombre + "';";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Nombre = (string)unDr.GetValue(0);

                }
                return aux.Nombre;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<string> nombreRefugio()
        {
            try
            {
                List<string> aux = new List<string>();
                string sql = "Select nombre from Refugios;";
                SqlDataReader unDr = ejecutarReader(sql);
                while (unDr.Read())
                {
                    aux.Add((string)unDr.GetValue(0));

                }
                return aux;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

         
    }
}