﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Interfaz/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="Listados.aspx.cs" Inherits="ObPro3.Interfaz.Listados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>

        <br />
        <br />
        <asp:Button ID="btnAnimalesEnAdopcion" runat="server" OnClick="btnAnimalesEnAdopcion_Click" Text="ANIMALES EN ADOPCION" />
&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="ddlEstado" runat="server">
            <asp:ListItem Value="INICIADA">INICIADA</asp:ListItem>
            <asp:ListItem Value="EN_PROCESO">EN_PROCESO</asp:ListItem>
            <asp:ListItem Value="COMPLETADA">COMPLETADA</asp:ListItem>
            <asp:ListItem Value="RECHAZADA">RECHAZADA</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="btnRefugios" runat="server" Text="REFUGIOS" OnClick="btnRefugios_Click" />
        <br />
        <br />
        <asp:Button ID="btnPadrinos" runat="server"  Text="PADRINOS" OnClick="btnPadrinos_Click" />
        <br />
        <asp:GridView ID="gvListados" runat="server">
        </asp:GridView>
        <br />
        <br />
        <br />
        <br />

    </div>
</asp:Content>
