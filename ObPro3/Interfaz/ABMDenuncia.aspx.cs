﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ObPro3.Interfaz
{
    public partial class ABMDenuncia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if ((string)Session["Rol"] == "Empleado" || (string)Session["Rol"] == "Admin")
            //{
            //    txtId.Visible = true;
            //    btnBuscar.Visible = true;
            //    chkEstado.Visible = true;
            //}
            //else
            //{
            //    txtId.Visible = false;
            //    btnBuscar.Visible = false;
            //    chkEstado.Visible = false;
            //}
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Ong.getInstancia().altaDenuncia(int.Parse(txtTelefono.Text), txtEmail.Text, txtDescripcion.Text, chkEstado.Checked);
            }
            catch
            {
                lblError.Text = "Los datos son incorrectos";
            }
        }
    }
}