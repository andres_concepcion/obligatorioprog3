﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObPro3.Persistencia;
using ObPro3.Dominio;
using System.Data;
using System.Data.SqlClient;

namespace ObPro3.Interfaz
{
    public partial class ABMAnimales : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> lstAux = Ong.getInstancia().nombreRefugio();
            ddlRefugios.DataSource = lstAux;
            ddlRefugios.DataBind();
        }
        /*private void LlenarDDL()
        {
            string ConnectionString = @"Data Source =THEWITCHER\SQLEXPRESS; Initial Catalog = Ongs; Integrated Security = True";
            using (SqlConnection cn = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter adp = new SqlDataAdapter("Select nombre from Refugios;", cn);
                DataTable dt = new DataTable();

                adp.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    ddlRefugios.DataSource = dt;
                    ddlRefugios.DataBind();
                }
            }
        }*/
        

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            bool cas = false;
            bool vac = false;

            if (cbCastracion.Checked) { cas = true; }
            if (cbVacunas.Checked) { vac = true; }


                Ong.getInstancia().altaAnimal(txtNombre.Text, txtEspecie.Text, int.Parse(txtTamanio.Text), int.Parse(txtPeso.Text), decimal.Parse(txtEdad.Text), vac, cas, txtEstadoSanitario.Text, ddlRefugios.SelectedValue);
            
                //Ong.getInstancia().modificarAnimal(int.Parse(txtId.Text),txtNombre.Text, txtEspecie.Text, int.Parse(txtTamanio.Text), int.Parse(txtPeso.Text), decimal.Parse(txtEdad.Text), vac, cas, txtEstadoSanitario.Text, ddlRefugios.SelectedValue);
            
        }
        
        protected void ddlRefugios_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            
            Animal animal = Ong.getInstancia().buscarAnimalPorId(int.Parse(txtId.Text));

            txtNombre.Text = animal.Nombre;
            txtEspecie.Text = animal.Especie;
            txtTamanio.Text = animal.Tamanio.ToString();
            txtPeso.Text = animal.Peso.ToString();
            txtEdad.Text = animal.Edad.ToString();
            txtEstadoSanitario.Text = animal.EstadoSanitario ;
            ddlRefugios.SelectedValue = animal.Refugio;
            



        }
    }
}