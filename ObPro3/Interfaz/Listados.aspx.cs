﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObPro3.Persistencia;
using ObPro3.Dominio;

namespace ObPro3.Interfaz
{
    public partial class Listados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAnimalesEnAdopcion_Click(object sender, EventArgs e)
        {
            List<Animal> lstAux = Ong.getInstancia().reporteAnimalesEnAdopcionPorEstado(ddlEstado.SelectedValue);
            gvListados.DataSource = lstAux;
            gvListados.DataBind();
        }

        protected void btnRefugios_Click(object sender, EventArgs e)
        {
            List<Refugio> lstAux = Ong.getInstancia().reporteRefugios();
            gvListados.DataSource = lstAux;
            gvListados.DataBind();
        }

        protected void btnPadrinos_Click(object sender, EventArgs e)
        {
            List<Padrino> lstAux = Ong.getInstancia().reporteAdopcionesPorPadrino();
            gvListados.DataSource = lstAux;
            gvListados.DataBind();
        }
    }
}