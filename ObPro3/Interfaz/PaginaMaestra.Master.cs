﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ObPro3.Interfaz
{
    public partial class PaginaMaestra : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           /* //if (!IsPostBack)
            //{
            //    Session["Usuario"] = "";
            //    Session["Rol"] = "";
            //    Session["Nombre"] = "";
            //}
           
            if ((string)Session["Rol"] == "Empleado" || (string)Session["Rol"] == "Admin")
            {
                ABManimales.Visible = true;
                ABMrefugios.Visible = true;
                listados.Visible = true;
            }
            else
            {
                ABManimales.Visible = false;
                ABMrefugios.Visible = false;
                listados.Visible = false;
            }
            if (Session["Usuario"] == null)
            {
                inicioSesion.Visible = true;
                registro.Visible = true;
                desconectarse.Visible = false;
            }
            else
            {
                inicioSesion.Visible = false;
                registro.Visible = false;
                desconectarse.Visible = true;
            }*/
        }
        protected void desconectarse_Click(object sender, EventArgs e)
        {
            Session["Usuario"] = "";
            Session["Rol"] = "";
            Session["Nombre"] = "";
            Response.Redirect("~/Interfaz/Inicio.aspx");
        }
    }
}