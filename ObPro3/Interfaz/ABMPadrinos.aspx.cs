﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ObPro3.Interfaz
{
    public partial class ABMPadrinos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if((string)Session["Rol"]=="Empleado"||(string)Session["Rol"]=="Admin")
            //{
            //    btnBuscar.Visible = true;
            //    btnModificar.Visible = true;
            //    ddlEstado.Visible = true;
            //}
            //else
            //{
            //    btnBuscar.Visible = false;
            //    btnModificar.Visible = false;
            //    ddlEstado.Visible = false;
            //}
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ObPro3.Dominio.Padrino aux = Ong.getInstancia().buscarPadrinoPorCi(int.Parse(txtCi.Text));
                if (aux != null)
                {
                    txtNombre.Text = aux.Nombre;
                    txtTelefono.Text = aux.Telefono.ToString();
                    txtEmail.Text = aux.Email;
                    txtDireccion.Text = aux.Direccion;
                }
                else if (aux == null)
                {
                    lblError.Text = "No existe el padrino en el sistema";
                }
            }
            catch
            {
                lblError.Text = "Error en el ingreso de datos";
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                bool aux = Ong.getInstancia().altaPadrino(int.Parse(txtCi.Text), txtNombre.Text, int.Parse(txtTelefono.Text), txtEmail.Text, txtDireccion.Text);
                if (aux == true)
                {
                    Ong.getInstancia().registroUsuario(int.Parse(txtCi.Text), txtPassword.Text, "Padrino");
                }
                else
                {
                    lblError.Text = "Ya existe el padrino en el sistema";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Error en el ingreso de datos";
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                bool aux = Ong.getInstancia().modificarPadrino(int.Parse(txtCi.Text),txtNombre.Text, int.Parse(txtTelefono.Text), txtEmail.Text, txtDireccion.Text,(Dominio.Estado)Enum.Parse(typeof(Dominio.Estado),ddlEstado.SelectedValue));
                if (aux == true)
                {
                    Ong.getInstancia().cambiarContrasenia(int.Parse(txtCi.Text), txtPassword.Text);
                }
                else
                {
                    lblError.Text = "El padrino no fue modificado";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Error en el ingreso de datos";
            }
        }
    }
}