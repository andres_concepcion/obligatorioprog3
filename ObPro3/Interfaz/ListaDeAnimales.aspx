﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Interfaz/PaginaMaestra.Master" AutoEventWireup="true" CodeBehind="ListaDeAnimales.aspx.cs" Inherits="ObPro3.Interfaz.ListaDeAnimales" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>


        <br />
        <asp:Label ID="lblTituloListadosAnimales" runat="server" Font-Size="XX-Large" Text="Listados Animales"></asp:Label>
        <br />
        <asp:Button ID="btnCargarListaAnimales" runat="server" OnClick="btnCargarListaAnimales_Click" Text="Cargar Animales" />
        <br />
        <asp:GridView ID="gvListaAnimales" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:ButtonField CommandName="Adoptar" Text="Adoptar" />
                <asp:ButtonField CommandName="Modificar" Text="Modificar" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />


    </div>
</asp:Content>
