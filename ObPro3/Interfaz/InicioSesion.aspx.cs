﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ObPro3.Interfaz
{
    public partial class InicioSesion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                string aux = Ong.getInstancia().inicioSesion(int.Parse(txtUsuario.Text), txtContrasenia.Text);
                if (aux != "")
                {
                    Session["Usuario"] = txtUsuario.Text;
                    Session["Rol"] = aux;
                    if (aux == "Padrino")
                    {
                        Session["Nombre"] = Ong.getInstancia().buscarPadrinoPorCi(int.Parse(txtUsuario.Text)).Nombre;
                        //Response.Redirect("~/Interfaz/Inicio.aspx");
                    }
                    else if (aux == "Empleado")
                    {
                        Session["Nombre"] = Ong.getInstancia().buscarEmpleadoPorId(int.Parse(txtUsuario.Text)).Nombre;
                        //Response.Redirect("~/Interfaz/Inicio.aspx");
                    }
                    else if (aux == "Admin")
                    {
                        Session["Nombre"] = Ong.getInstancia().buscarEmpleadoPorId(int.Parse(txtUsuario.Text)).Nombre;
                        //Response.Redirect("~/Interfaz/Inicio.aspx");
                    }
                }
                else
                {
                    lblError.Text = "El suario no existe";
                }
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }
    }
}