﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ObPro3.Interfaz
{
    public partial class ADOPTAR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if ((string)Session["Rol"] == "Empleado" || (string)Session["Rol"] == "Admin")
            //{
            //    ddlEstado.Visible = true;
            //    btnModificar.Visible = true;
            //}
            //else
            //{
            //    ddlEstado.Visible = false;
            //    btnModificar.Visible = false;
            //}
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Ong.getInstancia().altaAdopcion(DateTime.Now, Ong.getInstancia().buscarAnimalPorId(int.Parse(txtIdAnimal.Text)), Ong.getInstancia().buscarPadrinoPorCi(int.Parse(txtCiPadrino.Text)), Ong.getInstancia().buscarEmpleadoPorId(int.Parse(txtCiEmpleado.Text)));
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                Ong.getInstancia().modificarAdopcion(int.Parse(txtId.Text), (Dominio.Adopcion.EstadoAdopcion)Enum.Parse(typeof(Dominio.Adopcion.EstadoAdopcion), ddlEstado.SelectedValue.ToString()), Ong.getInstancia().buscarAnimalPorId(int.Parse(txtIdAnimal.Text)), Ong.getInstancia().buscarPadrinoPorCi(int.Parse(txtCiPadrino.Text)), Ong.getInstancia().buscarEmpleadoPorId(Convert.ToInt32(Session["Usuario"])));
            }
            catch
            {
                lblError.Text = "Los datos no son correctos";
            }
        }
    }
}